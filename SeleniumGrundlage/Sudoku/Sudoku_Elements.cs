﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace SeleniumGrundlage
{
    public static class Sudoku_Elements
    {
        public static readonly By StartButton = 
            By.CssSelector("#overlay-wrapper .sudoku-start-btn");

        public static readonly By SuperHard =
            By.CssSelector(".difficulty-links a[href=\"/sudoku-sehr-schwer/\"]");

        public static readonly By EmptySpace =
            By.CssSelector("#overlay-wrapper .sudoku .empty");

        public static readonly By Timer =
            By.CssSelector(".timer");
    }
}
