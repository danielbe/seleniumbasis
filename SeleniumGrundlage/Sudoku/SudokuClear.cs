﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace SeleniumGrundlage
{
    class SudokuClear : Setup
    {
        public string Start()
        {
            Driver.Navigate().GoToUrl("http://sudoku.tagesspiegel.de/");
            Click(Sudoku_Elements.SuperHard);
            Click(Sudoku_Elements.StartButton);

            IReadOnlyCollection<IWebElement> emptySpaces 
                = Driver.FindElements(Sudoku_Elements.EmptySpace);

            foreach (IWebElement emptySpace in emptySpaces)
            {
                //Click(emptySpace);
                Type(emptySpace, emptySpace.GetAttribute("data-solution"));
            }

            return Driver.FindElement(Sudoku_Elements.Timer).Text;
        }
    }
}
