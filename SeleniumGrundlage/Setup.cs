﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace SeleniumGrundlage
{
    public class Setup
    {
        public static IWebDriver Driver;
        public static WebDriverWait Wait;

        public Setup()
        {
            Driver = new ChromeDriver();
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
        }

        public void Click(By element)
        {
            Wait.Until(ExpectedConditions.ElementToBeClickable(element));
            Driver.FindElement(element).Click();
        }

        public void Click(IWebElement element)
        {
            Wait.Until(ExpectedConditions.ElementToBeClickable(element));
            element.Click();
        }

        public void Type(IWebElement element, string text)
        {
            Wait.Until(ExpectedConditions.ElementToBeClickable(element));
            element.SendKeys(text);
        }
    }
}
